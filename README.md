# GitLab Cloud Connector clients

This repository contains Cloud Connector clients to harness authentications and authorizations of [the backend services](https://docs.gitlab.com/ee/development/cloud_connector/architecture.html), GitLab-Rails and Customer Dot. For more information, see the following issues.

- [Cloud Connector: multi-backend support](https://gitlab.com/gitlab-org/gitlab/-/issues/468196)
- [Extract a Cloud Connector code module](https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist/-/issues/537)

## APIs

| API                                             | Description                                                     | Ruby client        | Python client      | Go client          |
| ---                                             | ------------                                                    | ------------------ | ------------------ | ------------------ |
| Get service                                     | Get a [service](#service-entries) details.                      |                    | :white_check_mark: |                    |
| Get bundle                                      | Get a [bundle](#bundle-entries) details.                        |                    | :white_check_mark: |                    |
| List unit primitives                            | Get a list of [unit primitives](#unit-primitive-entries).       |                    | :white_check_mark: |                    |

See [clients folder](https://gitlab.com/shinya.maeda/poc-cloud-connector/-/tree/master/clients) for more information.

## Service entries

Backend service entries are managed in the [services](https://gitlab.com/shinya.maeda/poc-cloud-connector/-/tree/master/assets/services) folder.
The structure of an entry is the following:

| Field             | Type      | Description                                                        | Required  | Constraint                                                                       |
| ----------        | --------- | --------------------                                               | --------- | -------------------------------------------                                      |
| name              | String    | Name of the service.                                               | Yes       | Length is between 5 and 20. Must be unique.                                      |
| project_url       | String    | URL of the project that maintains the application code.            | Yes       |                                                                                  |
| group             | String    | GitLab Group name that owns the service.                           | No        |                                                                                  |
| jwt:aud:          | String    | `aud` field of the JWT issued by the Cloud Connector token issuer. | Yes       | Must be unique.                                                                  |
| jwt:scopes:       | Array\<String\> | `scopes` field of the JWT issued by the Cloud Connector token issuer.       | Yes       | Must be included in [the registered unit primitives](#unit-primitive-entries).       |

## Bundle entries

Bundle entries are managed in the [bundles](https://gitlab.com/shinya.maeda/poc-cloud-connector/-/tree/master/assets/bundles) folder.
The structure of an entry is the following:

| Field             | Type      | Description                                               | Required  | Constraint                                                                           |
| ----------        | --------- | --------------------                                      | --------- | -------------------------------------------                                          |
| name              | String    | Name of the bundle                                        | Yes       | Length is between 5 and 20. Must be unique.                                          |
| unit_primitives   | Array\<String\> | List of unit primitives included in the bundle.     | Yes       | Must be included in [the registered unit primitives](#unit-primitive-entries).       |

## Unit Primitive entries

Unit Primitive entries are managed in the [unit_primitives](https://gitlab.com/shinya.maeda/poc-cloud-connector/-/tree/master/assets/unit_primitives) folder.
The structure of an entry is the following:

| Field             | Type      | Description                              | Required  | Constraint                                                                           |
| ----------        | --------- | --------------------                     | --------- | -------------------------------------------                                          |
| name              | String    | Name of the unit primitive               | Yes       | Length is between 5 and 20. Must be unique.                                          |
| description       | String    | Description of the unit primitive        | Yes       | Length is between 10 and 500.                                                        |
| cut_off_date      | Datetime  | Scheduled date to cut off free access of the unit primitive                                       | No  | |
| min_gitlab_version | String   | Minimum required version of the GitLab-Rails to run the feature.                                  | Yes | |
| min_service_version | String  | Minimum required version of the backend service to run the feature.                               | Yes | |
| feature_issue_url | String | Issue URL that proposed to add the unit primitive in product.                                        | Yes | |
| introduced_by_url | String | MR URL that adds the unit primitive in code.                                                         | Yes | |
| feature_category  | String | Feature Category of the unit primitive. | Yes | Must be one of [the registered categories in the `stages.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/data/stages.yml) |
| group             | String | GitLab Group name that owns the feature category and unit primitive. | Yes | |

### Unit Primitive registration

Merging a change to `main` branch in this project automatically publishes a new package to the package registries,
which later you can access to them by bumping the client version in a backend service repo.
It's advised to setup [a renovation bot](https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/renovate-bot.md) to automate the process,
so that your backend service always uses the latest UP definitions.

For backward compatibilities, the same version of cloud connector clients should be used across GitLab Rails, Customer Dots and Backend services.
Removing or renaming existing unit primitives might break a backward compatibility, hence, should be handled carefully.
