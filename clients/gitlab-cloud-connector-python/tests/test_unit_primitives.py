import pytest
from gitlab_cloud_connector.unit_primitives import list_unit_primitives
from gitlab_cloud_connector.models import UnitPrimitive

def test_list_unit_primitives():
    results = list_unit_primitives()
    assert isinstance(results, dict)

    for key, unit_primitive in results.items():
        assert isinstance(key, str)
        assert isinstance(unit_primitive, UnitPrimitive)

def test_list_unit_primitives_with_service_name():
    results = list_unit_primitives(service_name='ai_gateway')
    assert isinstance(results, dict)
    assert len(results) > 0

def test_list_unit_primitives_with_unknown_service_name():
    with pytest.raises(KeyError):
        list_unit_primitives(service_name='unknown')

def test_list_unit_primitives_with_bundle_name():
    results = list_unit_primitives(bundle_name='duo_pro')
    assert isinstance(results, dict)
    assert len(results) > 0

def test_list_unit_primitives_with_unknown_bundle_name():
    with pytest.raises(KeyError):
        list_unit_primitives(bundle_name='unknown')
