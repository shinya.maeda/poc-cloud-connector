import pytest
from gitlab_cloud_connector.services import get_service
from gitlab_cloud_connector.models import Service, UnitPrimitive

def test_get_service():
    results = get_service(name='ai_gateway')
    assert isinstance(results, Service)

    for key, scope in results.jwt.scopes.items():
        assert isinstance(key, str)
        assert isinstance(scope, UnitPrimitive)

def test_get_service_unknown():
    with pytest.raises(KeyError):
        get_service(name='unknown')
