import pytest
from gitlab_cloud_connector.bundles import get_bundle
from gitlab_cloud_connector.models import Bundle, UnitPrimitive

def test_get_bundle():
    results = get_bundle(name='duo_pro')
    assert isinstance(results, Bundle)

    for key, unit_primitive in results.unit_primitives.items():
        assert isinstance(key, str)
        assert isinstance(unit_primitive, UnitPrimitive)

def test_get_bundle_unknown():
    with pytest.raises(KeyError):
        get_bundle(name='unknown')
