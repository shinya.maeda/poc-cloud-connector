# Cloud Connector Python client

Move to the client directory:

```shell
cd clients/gitlab-cloud-connector-python
```

Run locally:

```shell
poetry run python
```

```python
from gitlab_cloud_connector.services import get_service
from gitlab_cloud_connector.bundles import get_bundle
from gitlab_cloud_connector.unit_primitives import list_unit_primitives

get_service(name='ai_gateway')

# Service(name='ai_gateway', project_url='https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist', group='group::ai framework', jwt=JWT(aud='gitlab-ai-gateway', scopes={'duo_chat': UnitPrimitive(name='duo_chat', description='This is awesome feature.', cut_off_date=None, min_gitlab_version='17.0', min_service_version='1.4.0', feature_issue_url='https://gitlab.com/groups/gitlab-org/-/epics/13963', introduced_by_url='https://gitlab.com/gitlab-org/gitlab/-/merge_requests/154083', feature_category='duo_chat', group='group::duo chat')}

get_bundle(name='duo_pro')

# Bundle(name='duo_pro', unit_primitives={'duo_chat': UnitPrimitive(name='duo_chat', description='This is awesome feature.', cut_off_date=None, min_gitlab_version='17.0', min_service_version='1.4.0', feature_issue_url='https://gitlab.com/groups/gitlab-org/-/epics/13963', introduced_by_url='https://gitlab.com/gitlab-org/gitlab/-/merge_requests/154083', feature_category='duo_chat', group='group::duo chat')})

list_unit_primitives(service_name='ai_gateway')

# > {'duo_chat': UnitPrimitive(name='duo_chat', description='This is awesome feature.', cut_off_date=None, min_gitlab_version='17.0', min_service_version='1.4.0', feature_issue_url='https://gitlab.com/groups/gitlab-org/-/epics/13963', introduced_by_url='https://gitlab.com/gitlab-org/gitlab/-/merge_requests/154083', feature_category='duo_chat', group='group::duo chat')}
```

Run test:

```shell
make test
```

Build a package:

```shell
make build
```

Publish a package:

```shell
make publish
```

Copy Cloud Connector assets (This automatically runs before test and build):

```shell
make cloud-connector-assets
```
