from functools import lru_cache

from gitlab_cloud_connector.models import UnitPrimitive
from gitlab_cloud_connector.utils import (parsed_bundles, parsed_services,
                                          parsed_unit_primitives)


@lru_cache
def list_unit_primitives(
    service_name: str = None, bundle_name: str = None
) -> dict[str, UnitPrimitive]:
    """
    List unit primitives

    Args:
        service_name: Filter by the specified service name
    """
    result = {}

    for name, content in parsed_unit_primitives().items():
        if (
            service_name is not None
            and name not in parsed_services()[service_name]["jwt"]["scopes"]
        ):
            continue

        if (
            bundle_name is not None
            and name not in parsed_bundles()[bundle_name]["unit_primitives"]
        ):
            continue

        result[name] = UnitPrimitive(**content)

    return result
