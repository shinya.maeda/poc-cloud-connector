import glob
from functools import lru_cache

import yaml


def read_yaml_files_in_directory(directory):
    yaml_contents = {}
    files = glob.glob(f"{directory}/*.yml")
    for file in files:
        with open(file, "r") as stream:
            try:
                yaml_contents[file] = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(f"Error reading {file}: {exc}")
    return yaml_contents


@lru_cache
def parsed_unit_primitives() -> dict:
    data = {}

    yaml_files_content = read_yaml_files_in_directory(
        "gitlab_cloud_connector/assets/unit_primitives"
    )
    for filename, content in yaml_files_content.items():
        try:
            data[content["name"]] = content
        except Exception as exc:
            print(f"Error reading {filename}: {exc}")
            raise

    return data


@lru_cache
def parsed_services() -> dict:
    data = {}

    yaml_files_content = read_yaml_files_in_directory(
        "gitlab_cloud_connector/assets/services"
    )
    for filename, content in yaml_files_content.items():
        try:
            data[content["name"]] = content
        except Exception as exc:
            print(f"Error reading {filename}: {exc}")
            raise

    return data


@lru_cache
def parsed_bundles() -> dict:
    data = {}

    yaml_files_content = read_yaml_files_in_directory(
        "gitlab_cloud_connector/assets/bundles"
    )
    for filename, content in yaml_files_content.items():
        try:
            data[content["name"]] = content
        except Exception as exc:
            print(f"Error reading {filename}: {exc}")
            raise

    return data
