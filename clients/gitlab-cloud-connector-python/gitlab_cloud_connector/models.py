from datetime import datetime
from typing import Optional

from pydantic import BaseModel


class UnitPrimitive(BaseModel):
    name: str
    description: str
    cut_off_date: Optional[datetime]
    min_gitlab_version: str
    min_service_version: str
    feature_issue_url: str
    introduced_by_url: str
    feature_category: str
    group: str


class JWT(BaseModel):
    aud: str
    scopes: dict[str, UnitPrimitive]


class Service(BaseModel):
    name: str
    project_url: str
    group: str
    jwt: JWT


class Bundle(BaseModel):
    name: str
    unit_primitives: dict[str, UnitPrimitive]
