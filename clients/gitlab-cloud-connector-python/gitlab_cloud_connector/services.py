from functools import lru_cache

from gitlab_cloud_connector.models import Service, UnitPrimitive
from gitlab_cloud_connector.utils import (parsed_services,
                                          parsed_unit_primitives)


@lru_cache
def get_service(name: str) -> Service:
    """
    Get a service details

    Args:
        name: Name of the service.
    """
    content = parsed_services()[name]

    content["jwt"]["scopes"] = {
        scope: UnitPrimitive(**parsed_unit_primitives()[scope])
        for scope in content["jwt"]["scopes"]
    }

    return Service(**content)
