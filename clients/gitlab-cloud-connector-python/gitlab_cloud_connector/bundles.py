from functools import lru_cache

from gitlab_cloud_connector.models import Bundle, UnitPrimitive
from gitlab_cloud_connector.utils import parsed_bundles, parsed_unit_primitives


@lru_cache
def get_bundle(name: str) -> Bundle:
    """
    Get a bundle details

    Args:
        name: Name of the bundle.
    """
    content = parsed_bundles()[name]

    content["unit_primitives"] = {
        scope: UnitPrimitive(**parsed_unit_primitives()[scope])
        for scope in content["unit_primitives"]
    }

    return Bundle(**content)
